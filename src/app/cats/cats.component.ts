import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {CatService} from "../shared/services/cat.service";
import {BreedModel} from "../shared/models/breed.model";
import {FavouriteModel} from "../shared/models/favourite.model";
import {VotesModel} from "../shared/models/votes.model";
import Swal from 'sweetalert2';


@Component({
  selector: 'app-cats',
  templateUrl: './cats.component.html',
  styleUrls: ['./cats.component.scss']
})
export class CatsComponent implements OnInit {
  @ViewChild('button') button: ElementRef;

  listOfBreeds: boolean = false;
  catBreeds: BreedModel[] = [];
  favouriteBreeds: BreedModel[] = [];
  favourite: FavouriteModel[] = [];
  votes: VotesModel[] = []

  constructor(private catService: CatService) {
  }

  ngOnInit() {
    this.catService.getVotes().subscribe(res => {
      this.votes = res
      console.log(res)
    })
  }

  getFavouriteBreeds() {
    this.catService.getFavouriteBreeds().subscribe(res => {
      this.favourite = res;
      let itemsToRemove = new Set(res.map(x => x.image_id));
      this.catBreeds = this.catBreeds.filter(x => {
        itemsToRemove.has(x.image?.id) ? this.favouriteBreeds.push(x) : null;
        return !itemsToRemove.has(x.image?.id);
      })
    })
  }

  getBreeds() {
    this.catService.getCatBreeds().subscribe(res => {
      this.catBreeds = res;
      this.listOfBreeds = true;
      this.getFavouriteBreeds()
    })
  }

  onSelect(event, id, breedId) {
    this.catService.addFavouriteBreed(id).subscribe(res => {
      this.favouriteBreeds.push(this.catBreeds.find(x => x.id == breedId))
      this.catBreeds = this.catBreeds.filter(x => x.image?.id != id)
    })
  }


  voteFavorite(type: 'like' | 'dislike' = 'like', item: BreedModel) {
    let itemImageId = item.image?.id;
    if (type == 'like') {
      this.catService.voteFavorite(itemImageId, this.getVotesForItem(itemImageId) + 1).subscribe(res => {
        this.ngOnInit()
      })
    } else {
      this.catService.disslikeFavorite(this.votes.find(x => x.image_id == itemImageId).id).subscribe(res => {
        console.log(res, 'fav')
        this.ngOnInit()
      })
    }
  }

  getVotesForItem(itemImageId: string) {
    return this.votes.find(x => x.image_id == itemImageId)?.value
  }

  removeFavorite(favBreed: BreedModel) {
    Swal.fire({
      title: 'Are you sure?',
      text: "This breed will removed from your favourite list",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, remove it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.catService.deleteFavorite(this.favourite.find(x => x.image_id == favBreed.image?.id)?.id).subscribe(res => {
          this.favouriteBreeds = this.favouriteBreeds.filter(x => x.id !== favBreed?.id);
          this.catBreeds.push(favBreed);
        })
        Swal.fire(
          'Removed!',
          'This breed has been removed.',
          'success'
        )
      }
    })
  }
}
