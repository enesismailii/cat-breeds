import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatsRoutingModule } from './cats-routing.module';
import {AppModule} from "../app.module";


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CatsRoutingModule,
    AppModule
  ]
})
export class CatsModule { }
