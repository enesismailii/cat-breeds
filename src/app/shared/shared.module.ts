import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ListItemComponent} from "./components/list-item/list-item.component";
import {MatListModule} from "@angular/material/list";



@NgModule({
  declarations: [ListItemComponent],
  imports: [
    CommonModule,
    MatListModule
  ],
  exports: [
    ListItemComponent
  ]
})
export class SharedModule { }
