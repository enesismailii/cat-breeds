import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import {BreedModel} from "../../models/breed.model";

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {
  @Input() breed: BreedModel;
  @Output() onSelect: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }

  ngOnInit(): void {
  }

}
