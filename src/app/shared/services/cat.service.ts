import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {BreedModel} from "../models/breed.model";
import {FavouriteModel} from "../models/favourite.model";
import {ImageModel} from "../models/image.model";
import {VotesModel} from "../models/votes.model";

@Injectable({
  providedIn: 'root'
})
export class CatService {

  constructor(private http: HttpClient) {
  }

  getCatBreeds() {
    return this.http.get<BreedModel[]>(environment.apiUrl + '/v1/breeds')
  }

  getFavouriteBreeds() {
    return this.http.get<FavouriteModel[]>(environment.apiUrl + '/v1/favourites')
  }

  addFavouriteBreed(id: string) {
    return this.http.post(environment.apiUrl + '/v1/favourites', {"image_id": id, "sub_id": environment.xApiKey})
  }

  voteFavorite(itemImageId: string, value = 1) {
    return this.http.post(environment.apiUrl + '/v1/votes', {
      'image_id': itemImageId,
      'sub_id': environment.xApiKey,
      'value': 1
    })
  }

  disslikeFavorite(voteId: string) {
    return this.http.delete(environment.apiUrl + `/v1/votes/${voteId}`)
  }

  getVotes() {
    return this.http.get<VotesModel[]>(environment.apiUrl + '/v1/votes',)
  }

  deleteFavorite(favId: string) {
    return this.http.delete(environment.apiUrl + `/v1/favourites/${favId}`);
  }
}
